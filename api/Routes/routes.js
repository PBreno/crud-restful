const express = require ('Express')
const router = express.Router()
const oficinaRoute =require('./oficina')
const localRoute =require('./local')
const usuarioRoute = require('./usuario')

router.use('/oficina', oficinaRoute)
router.use('/local', localRoute)
router.use('/usuarios', usuarioRoute)

module.exports = router