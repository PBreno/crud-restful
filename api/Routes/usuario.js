const express = require('express')
const route = express.Router()
let data = require('../data')

route.get('/', (req, res)=>{

    res.json(data.usuarios)
})

route.get('/:id', (req,res)=>{

    let id = req.params.id

    const elementID = usuario => usuario._id == id
    const elementFiltered= data.usuarios.filter(elementID) 
    
    res.status(200).send(elementFiltered) 
})


route.post('/', (req, res) =>{

    let usuarios = req.body

    data.usuarios.push(usuarios)

    res.json(usuarios)
})

route.put('/:id', (req, res) =>{

    
    let id = req.params.id
    let updated = req.body

    data.usuarios[id] = updated

    res.json(updated)
})

route.delete('/:id', (req, res) =>{

    let id = req.params.id
    let deleted = data.usuarios.splice(id,1)
    res.json(deleted)
})


module.exports = route