const express = require('express')
const route = express.Router()
let data = require('../data')

route.get('/', (req, res)=>{

    res.json(data.local)
})

route.get('/:id', (req,res)=>{

    let id = req.params.id

    const elementID = local => local._id == id
    const elementFiltered= data.local.filter(elementID) 
    
    res.status(200).send(elementFiltered) 
})


route.post('/', (req, res) =>{

    let local = req.body

    data.local.push(local)

    res.json(local)
})

route.put('/:id', (req, res) =>{

    
    let id = req.params.id
    let updated = req.body

    data.local[id] = updated

    res.json(updated)
})

route.delete('/:id', (req, res) =>{

    let id = req.params.id
    let deleted = data.local.splice(id,1)
    res.json(deleted)
})


module.exports = route