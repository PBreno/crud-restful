const express = require ('express')
const route = express.Router()
const data = require('../data')

route.get('/', (req, res) =>{

    if( JSON.stringify(data.oficina) == '{}' )
        res.status(404).send('There is no element in list!')
    else
        res.send(data.oficina)   
})

route.get('/:id', (req, res) =>{

    let id = req.params.id

    const elementID = oficina => oficina._id == id
    const elementFiltered= data.oficina.filter(elementID) 
    
    res.status(200).send(elementFiltered) 
   
})

route.post('/', (req, res) =>{

    let ofi = req.body

    data.oficina.push(ofi)

    res.json(ofi)
})

route.put('/:id', (req, res) =>{

    
    let id = req.params.id
    let updated = req.body

    data.oficina[id] = updated

    res.json(updated)
})

route.delete('/:id', (req, res) =>{

    let id = req.params.id
    let deleted = data.oficina.splice(id,1)
    res.json(deleted)
})

module.exports = route